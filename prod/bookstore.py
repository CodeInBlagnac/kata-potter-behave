class BookStore(object):
    def __init__(self, unitary_price):
        self.unitary_price = unitary_price
        self.nb_books_by_book_name = {}

    def add_to_cart(self, book_name):
        self.nb_books_by_book_name[book_name] = self.nb_books_by_book_name.get(book_name,0) +1

    def checkout(self):
        nb_total_books = sum(self.nb_books_by_book_name.values())
        total_price = nb_total_books * self.unitary_price
        nb_distinct_books = len(self.nb_books_by_book_name.values())
        if nb_distinct_books == 2:
            return total_price * 0.95
        elif nb_distinct_books == 3:
            return total_price * 0.9
        else:
            return total_price