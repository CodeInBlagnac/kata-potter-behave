# Created by moi at 20/09/18
Feature: Having discount when buying books

  Scenario: Buy a single book
    Given unit price is 8.
    And I put book A into my cart.
    When I checkout.
    Then the total price is 8.0.

  Scenario: Buy a single book with different initial price
    Given unit price is 9.
    And I put book A into my cart.
    When I checkout.
    Then the total price is 9.0.

  Scenario: Buy two times the same book
    Given unit price is 8.
    And I put book A into my cart.
    And I put book A into my cart.
    When I checkout.
    Then the total price is 16.0.

  Scenario: Buy three times the same book
    Given unit price is 8.
    And I put book C into my cart.
    And I put book C into my cart.
    And I put book C into my cart.
    When I checkout.
    Then the total price is 24.0.

  Scenario: Buy two different books
    Given unit price is 8.
    And I put book A into my cart.
    And I put book B into my cart.
    When I checkout.
    Then the total price is 15.2.

  Scenario: Buy three different books
    Given unit price is 8.
    And I put book A into my cart.
    And I put book B into my cart.
    And I put book C into my cart.
    When I checkout.
    Then the total price is 21.6.