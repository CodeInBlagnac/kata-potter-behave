from behave import *

from prod.bookstore import BookStore


@given("unit price is {unitary_price:d}.")
def step_impl(context, unitary_price):
    context.bookStore = BookStore(unitary_price)

@given("I put book {bookName:w} into my cart.")
def step_impl(context, bookName):
    context.bookStore.add_to_cart(bookName)

@when("I checkout.")
def step_impl(context):
    context.total_price = context.bookStore.checkout()

@then("the total price is {expected_total_price:f}.")
def step_impl(context, expected_total_price):
    print 'context.total_price=' + str(context.total_price) + ', expected_price=' + str(expected_total_price) + '\n'
    assert context.total_price == expected_total_price
